# -*- coding: utf-8 -*-

from models import *
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.authentication import SessionAuthentication, BasicAuthentication, MultiAuthentication
from tastypie.authorization import Authorization
from tastypie.bundle import Bundle
from tastypie.utils.urls import trailing_slash
from django.utils.translation import ugettext_lazy as _
from django.conf.urls import url

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Articles package"


class LibrarySectionResource(ModelResource):
    class Meta:
        queryset = LibrarySection.objects.all()
        resource_name = 'library_section'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', 'lft', 'level', 'tree_id', 'rght', ]
        allowed_methods = ['get', ]
        filtering = {
            "name": ALL,
            "slug": ALL,
            "articles": ALL_WITH_RELATIONS,
        }

    parent = fields.ToOneField('self', attribute='parent', null=True, blank=True, full=False, help_text=_('Parent'))
    name = fields.CharField(attribute='name', help_text=_('Name'))
    slug = fields.CharField(attribute='slug', help_text=_('Slug'))
    articles = fields.ToManyField('elastic.articles.api.ArticleResource', attribute='articles',
                                  null=True, blank=True, full=True, help_text=_('Articles'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<slug>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.slug
        else:
            kwargs['pk'] = bundle_or_obj.slug

        return kwargs


class ArticleResource(ModelResource):
    class Meta:
        queryset = Article.objects.all()
        resource_name = 'article'
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = Authorization()
        include_resource_uri = False
        excludes = ['id', ]
        allowed_methods = ['get', ]
        filtering = {
            "name": ALL,
            "slug": ALL,
            "is_active": ALL,
        }

    name = fields.CharField(attribute='name', help_text=_('Name'))
    slug = fields.CharField(attribute='slug', help_text=_('Slug'))
    is_active = fields.BooleanField(attribute='is_active', help_text=_('Is active'))
    creation_date = fields.DateTimeField(attribute='creation_date', help_text=_('Creation date'))
    text = fields.CharField(attribute='text', help_text=_('Text'))

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/schema%s$" % (self._meta.resource_name, trailing_slash()),
                self.wrap_view('get_schema'), name="api_get_schema"),
            url(r"^(?P<resource_name>%s)/(?P<slug>[\w\d_.-]+)/$" % self._meta.resource_name,
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.slug
        else:
            kwargs['pk'] = bundle_or_obj.slug

        return kwargs
