# -*- coding: utf-8 -*-

import autocomplete_light
from models import *

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Articles package"

autocomplete_light.register(Article, search_fields=('name',),
                            autocomplete_js_attributes={'placeholder': 'Статья....'})
