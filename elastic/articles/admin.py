# -*- coding: utf-8 -*-

from django.contrib import admin
from models import *
from mpttadmin.admin import MpttAdmin
from forms import LibrarySectionForm, ArticleForm

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Articles package"


class LibrarySectionAdmin(MpttAdmin):
    class Meta:
        model = LibrarySection

    tree_title_field = 'name'
    tree_display = ('name', )
    form = LibrarySectionForm
    prepopulated_fields = {"slug": ("name",)}

    fieldsets = (
        (None, {
            'fields':  ['parent', 'name', 'slug', 'articles', ]
        }),
    )

admin.site.register(LibrarySection, LibrarySectionAdmin)


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'creation_date', 'is_active', )
    form = ArticleForm
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(Article, ArticleAdmin)
