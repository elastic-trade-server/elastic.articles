# -*- coding: utf-8 -*-


from django.db import models
from django.utils.translation import ugettext_lazy as _
import mptt
from mptt.models import MPTTModel, TreeForeignKey
from taggit.managers import TaggableManager

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Articles package"


class Article(models.Model):
    """
    Статья
    """
    class Meta:
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')

    name = models.CharField(max_length=255, verbose_name=_('Name'))
    slug = models.SlugField(unique=True, verbose_name=_('Slug'))
    is_active = models.BooleanField(default=True, verbose_name=_('Is active'))
    creation_date = models.DateTimeField(verbose_name=_('Creation date'))
    text = models.TextField(verbose_name=_('Article text'))

    tags = TaggableManager()

    def __unicode__(self):
        return u"#{0} at {1}".format(self.name, self.creation_date.date())


class LibrarySection(MPTTModel):
    """
    Раздел библиотеки
    """
    class Meta:
        verbose_name = _('Library section')
        verbose_name_plural = _('Library sections')

    class MPTTMeta(object):
        parent_attr = 'parent'

    parent = TreeForeignKey('self', verbose_name=_('Parent'), null=True, blank=True, related_name='children')
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    slug = models.SlugField(unique=True, verbose_name=_('Slug'))
    articles = models.ManyToManyField(Article, blank=True, related_name='sections',
                                      verbose_name=_('Articles'))

    def __unicode__(self):
        return u"{0}".format(self.name)


mptt.register(LibrarySection)
