# -*- coding: utf-8 -*-

__author__ = "Igor S. Kovalenko"
__contact__ = "kovalenko@sb-soft.biz"
__site__ = "http://www.elastic-trade-server.org"
__year__ = "2015"
__description__ = "Articles package"

from django import forms
from models import *
from ckeditor.widgets import CKEditorWidget
import autocomplete_light
from django.utils.translation import ugettext_lazy as _


class ArticleForm(autocomplete_light.ModelForm):
    class Meta:
        model = Article
        fields = '__all__'
        autocomplete_exclude = ['text', ]

    text = forms.CharField(widget=CKEditorWidget(), required=True, label=_("Article text"))


class LibrarySectionForm(autocomplete_light.ModelForm):
    class Meta:
        model = LibrarySection
        fields = '__all__'
        autocomplete_exclude = ['parent', ]
